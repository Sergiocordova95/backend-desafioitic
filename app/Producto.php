<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Producto extends Eloquent
{
    protected $connection = 'mongodb';
	protected $collection = 'producto';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'categoria'
    ];
}
