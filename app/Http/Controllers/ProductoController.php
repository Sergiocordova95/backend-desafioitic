<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Producto;

class ProductoController extends Controller
{
    //

    public function index(){

        $producto = Producto::all();

        return view('producto.index', compact('producto'))
        ->with('i', request()->input('page,1'));
    
    }

    public function create(){
        
        return view('producto.create');

    }

    public function createProduct(Request $request){

        /*
        request()->validate([
            'nombre' => 'required',
            'categoria' => 'required',
        ]);
        */

        Producto::create($request->all());

        return response()->json('Producto Creado');

        /*
            return redirect()->route('producto.index')
            ->with('success','Producto Creado');
            */

    }


    public function show(Producto $producto){

        return view('producto.show', compact('producto'));


    }

    public function edit(Producto $producto){

        return view('producto.edit', compact('producto'));

    }


    public function update(Request $request, Producto $producto){

        request()->validate([
            'nombre' => 'required',
            'categoria' => 'required',
        ]);


        $producto->update($request->all());


        return redirect()->route('producto.index')
                        ->with('success','Producto Actualizado');

    }

    public function destroy(Producto $producto){

        $producto->delete();

        return redirect()->route('producto.index')
        ->with('success','Producto Eliminado');


    }


    public function exportarProductos(){


        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection();
        //$description = "Productos Registrados";

       // $section->addText($description);

       $producto = Producto::all();

       $table = $section->addTable();

       foreach ($producto as $prod) {
            $table->addRow();
            $table->addCell(1750)->addText("{$prod->nombre} - {$prod->categoria}");
        }
        
        

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path('Productos.docx'));
        } catch (Exception $e) {
        }


        return response()->download(storage_path('Productos.docx'));
        

        //print_r("doc");


    }
}
